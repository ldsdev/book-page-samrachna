/**
 * Load WPGulp Configuration.
 */
const config = module.exports = {

    // Project options.
    projectURL: 'Project URL', // Local project URL of your already running WordPress site. Could be something like wpgulp.local or localhost:3000 depending upon your local WordPress setup.
    productURL: './', // Theme/Plugin URL. Leave it like it is, since our gulpfile.js lives in the root folder.
    browserAutoOpen: false,
    injectChanges: true,

    // Style options.
    styleSRC: './assets/css/style.scss', // Path to main .scss file.
    styleDestination: './', // Path to place the compiled CSS file. Default set to root folder.
    outputStyle: 'compact', // Available options → 'compact' or 'compressed' or 'nested' or 'expanded'
    errLogToConsole: true,
    precision: 10,

    // JS Vendor options.
    jsVendorSRC: './assets/js/vendor/*.js', // Path to JS vendor folder.
    jsVendorDestination: './assets/js/', // Path to place the compiled JS vendors file.
    jsVendorFile: 'vendors', // Compiled JS vendors file name. Default set to vendors i.e. vendors.js.

    // JS Custom options.
    jsCustomSRC: './assets/js/custom/*.js', // Path to JS custom scripts folder.
    jsCustomDestination: './assets/js/', // Path to place the compiled JS custom scripts file.
    jsCustomFile: 'main', // Compiled JS custom file name. Default set to custom i.e. custom.js.

    // Watch files paths.
    watchStyles: './assets/css/**/*.scss', // Path to all *.scss files inside css folder and inside them.
    watchJsVendor: './assets/js/vendor/*.js', // Path to all vendor JS files.
    watchJsCustom: './assets/js/custom/*.js', // Path to all custom JS files.
    watchHTML: './**/*.html', // Path to all HTML files.

    // Browsers you care about for autoprefixing. Browserlist https://github.com/ai/browserslist
    // The following list is set as per WordPress requirements. Though, Feel free to change.
    BROWSERS_LIST: [
        'last 2 version',
        '> 1%',
        'ie >= 11',
        'last 1 Android versions',
        'last 1 ChromeAndroid versions',
        'last 2 Chrome versions',
        'last 2 Firefox versions',
        'last 2 Safari versions',
        'last 2 iOS versions',
        'last 2 Edge versions',
        'last 2 Opera versions'
    ]
};


/**
 * Load Plugins.
 *
 * Load gulp plugins and passing them semantic names.
 */
const gulp = require( 'gulp' ); // Gulp of-course.

// CSS related plugins.
const sass = require( 'gulp-sass' ); // Gulp plugin for Sass compilation.
const minifycss = require( 'gulp-uglifycss' ); // Minifies CSS files.
const autoprefixer = require( 'gulp-autoprefixer' ); // Autoprefixing magic.
const mmq = require( 'gulp-merge-media-queries' ); // Combine matching media queries into one.
const rtlcss = require( 'gulp-rtlcss' ); // Generates RTL stylesheet.

// JS related plugins.
const concat = require( 'gulp-concat' ); // Concatenates JS files.
const uglify = require( 'gulp-uglify' ); // Minifies JS files.
const babel = require( 'gulp-babel' ); // Compiles ESNext to browser compatible JS.

// Image related plugins.
const imagemin = require( 'gulp-imagemin' ); // Minify PNG, JPEG, GIF and SVG images with imagemin.

// Utility related plugins.
const rename = require( 'gulp-rename' ); // Renames files E.g. style.css -> style.min.css.
const lineec = require( 'gulp-line-ending-corrector' ); // Consistent Line Endings for non UNIX systems. Gulp Plugin for Line Ending Corrector (A utility that makes sure your files have consistent line endings).
const filter = require( 'gulp-filter' ); // Enables you to work on a subset of the original files by filtering them using a glob.
const sourcemaps = require( 'gulp-sourcemaps' ); // Maps code in a compressed file (E.g. style.css) back to it’s original position in a source file (E.g. structure.scss, which was later combined with other css files to generate style.css).
const notify = require( 'gulp-notify' ); // Sends message notification to you.
const browserSync = require( 'browser-sync' ).create(); // Reloads browser and injects CSS. Time-saving synchronized browser testing.
const wpPot = require( 'gulp-wp-pot' ); // For generating the .pot file.
const sort = require( 'gulp-sort' ); // Recommended to prevent unnecessary changes in pot-file.
const cache = require( 'gulp-cache' ); // Cache files in stream for later use.
const remember = require( 'gulp-remember' ); //  Adds all the files it has ever seen back into the stream.
const plumber = require( 'gulp-plumber' ); // Prevent pipe breaking caused by errors from gulp plugins.
const beep = require( 'beepbeep' );

/**
 * Custom Error Handler.
 *
 * @param Mixed err
 */
const errorHandler = r => {
    notify.onError( '\n\n❌  ===> ERROR: <%= error.message %>\n' )( r );
    beep();

    // this.emit('end');
};

/**
 * Task: `browser-sync`.
 *
 * Live Reloads, CSS injections, Localhost tunneling.
 * @link http://www.browsersync.io/docs/options/
 *
 * @param {Mixed} done Done.
 */
const browsersync = done => {
    browserSync.init({
        proxy: config.projectURL,
        open: config.browserAutoOpen,
        injectChanges: config.injectChanges,
        watchEvents: [ 'change', 'add', 'unlink', 'addDir', 'unlinkDir' ]
    });
    done();
};

// Helper function to allow browser reload with Gulp 4.
const reload = done => {
    browserSync.reload();
    done();
};

/**
 * Task: `styles`.
 *
 * Compiles Sass, Autoprefixes it and Minifies CSS.
 *
 * This task does the following:
 *    1. Gets the source scss file
 *    2. Compiles Sass to CSS
 *    3. Writes Sourcemaps for it
 *    4. Autoprefixes it and generates style.css
 *    5. Renames the CSS file with suffix .min.css
 *    6. Minifies the CSS file and generates style.min.css
 *    7. Injects CSS or reloads the browser via browserSync
 */
gulp.task( 'styles', () => {
    return gulp
        .src( config.styleSRC, { allowEmpty: true })
        .pipe( plumber( errorHandler ) )
        .pipe( sourcemaps.init() )
        .pipe(
            sass({
                errLogToConsole: config.errLogToConsole,
                outputStyle: config.outputStyle,
                precision: config.precision
            })
        )
        .on( 'error', sass.logError )
        .pipe( sourcemaps.write({ includeContent: false }) )
        .pipe( sourcemaps.init({ loadMaps: true }) )
        .pipe( autoprefixer( config.BROWSERS_LIST ) )
        .pipe( sourcemaps.write( './' ) )
        .pipe( lineec() ) // Consistent Line Endings for non UNIX systems.
        .pipe( gulp.dest( config.styleDestination ) )
        .pipe( filter( '**/*.css' ) ) // Filtering stream to only css files.
        .pipe( mmq({ log: true }) ) // Merge Media Queries only for .min.css version.
        .pipe( browserSync.stream() ) // Reloads style.css if that is enqueued.
        .pipe( rename({ suffix: '.min' }) )
        .pipe( minifycss({ maxLineLen: 10 }) )
        .pipe( lineec() ) // Consistent Line Endings for non UNIX systems.
        .pipe( gulp.dest( config.styleDestination ) )
        .pipe( filter( '**/*.css' ) ) // Filtering stream to only css files.
        .pipe( browserSync.stream() ) // Reloads style.min.css if that is enqueued.
        .pipe( notify({ message: '\n\n✅  ===> STYLES — completed!\n', onLast: true }) );
});


/**
 * Task: `stylesRTL`.
 *
 * Compiles Sass, Autoprefixes it, Generates RTL stylesheet, and Minifies CSS.
 *
 * This task does the following:
 *    1. Gets the source scss file
 *    2. Compiles Sass to CSS
 *    4. Autoprefixes it and generates style.css
 *    5. Renames the CSS file with suffix -rtl and generates style-rtl.css
 *    6. Writes Sourcemaps for style-rtl.css
 *    7. Renames the CSS files with suffix .min.css
 *    8. Minifies the CSS file and generates style-rtl.min.css
 *    9. Injects CSS or reloads the browser via browserSync
 */
gulp.task( 'stylesRTL', () => {
    return gulp
        .src( config.styleSRC, { allowEmpty: true })
        .pipe( plumber( errorHandler ) )
        .pipe( sourcemaps.init() )
        .pipe(
            sass({
                errLogToConsole: config.errLogToConsole,
                outputStyle: config.outputStyle,
                precision: config.precision
            })
        )
        .on( 'error', sass.logError )
        .pipe( sourcemaps.write({ includeContent: false }) )
        .pipe( sourcemaps.init({ loadMaps: true }) )
        .pipe( autoprefixer( config.BROWSERS_LIST ) )
        .pipe( lineec() ) // Consistent Line Endings for non UNIX systems.
        .pipe( rename({ suffix: '-rtl' }) ) // Append "-rtl" to the filename.
        .pipe( rtlcss() ) // Convert to RTL.
        .pipe( sourcemaps.write( './' ) ) // Output sourcemap for style-rtl.css.
        .pipe( gulp.dest( config.styleDestination ) )
        .pipe( filter( '**/*.css' ) ) // Filtering stream to only css files.
        .pipe( browserSync.stream() ) // Reloads style.css or style-rtl.css, if that is enqueued.
        .pipe( mmq({ log: true }) ) // Merge Media Queries only for .min.css version.
        .pipe( rename({ suffix: '.min' }) )
        .pipe( minifycss({ maxLineLen: 10 }) )
        .pipe( lineec() ) // Consistent Line Endings for non UNIX systems.
        .pipe( gulp.dest( config.styleDestination ) )
        .pipe( filter( '**/*.css' ) ) // Filtering stream to only css files.
        .pipe( browserSync.stream() ) // Reloads style.css or style-rtl.css, if that is enqueued.
        .pipe( notify({ message: '\n\n✅  ===> STYLES RTL — completed!\n', onLast: true }) );
});

/**
 * Task: `vendorsJS`.
 *
 * Concatenate and uglify vendor JS scripts.
 *
 * This task does the following:
 *     1. Gets the source folder for JS vendor files
 *     2. Concatenates all the files and generates vendors.js
 *     3. Renames the JS file with suffix .min.js
 *     4. Uglifes/Minifies the JS file and generates vendors.min.js
 */
gulp.task( 'vendorsJS', () => {
    return gulp
        .src( config.jsVendorSRC, { since: gulp.lastRun( 'vendorsJS' ) }) // Only run on changed files.
        .pipe( plumber( errorHandler ) )
        .pipe(
            babel({
                presets: [
                    [
                        '@babel/preset-env', // Preset to compile your modern JS to ES5.
                        {
                            targets: { browsers: config.BROWSERS_LIST } // Target browser list to support.
                        }
                    ]
                ]
            })
        )
        .pipe( remember( config.jsVendorSRC ) ) // Bring all files back to stream.
        .pipe( concat( config.jsVendorFile + '.js' ) )
        .pipe( lineec() ) // Consistent Line Endings for non UNIX systems.
        .pipe( gulp.dest( config.jsVendorDestination ) )
        .pipe(
            rename({
                basename: config.jsVendorFile,
                suffix: '.min'
            })
        )
        .pipe( uglify() )
        .pipe( lineec() ) // Consistent Line Endings for non UNIX systems.
        .pipe( gulp.dest( config.jsVendorDestination ) )
        .pipe( notify({ message: '\n\n✅  ===> VENDOR JS — completed!\n', onLast: true }) );
});

/**
 * Task: `customJS`.
 *
 * Concatenate and uglify custom JS scripts.
 *
 * This task does the following:
 *     1. Gets the source folder for JS custom files
 *     2. Concatenates all the files and generates custom.js
 *     3. Renames the JS file with suffix .min.js
 *     4. Uglifes/Minifies the JS file and generates custom.min.js
 */
gulp.task( 'customJS', () => {
    return gulp
        .src( config.jsCustomSRC, { since: gulp.lastRun( 'customJS' ) }) // Only run on changed files.
        .pipe( plumber( errorHandler ) )
        .pipe(
            babel({
                presets: [
                    [
                        '@babel/preset-env', // Preset to compile your modern JS to ES5.
                        {
                            targets: { browsers: config.BROWSERS_LIST } // Target browser list to support.
                        }
                    ]
                ]
            })
        )
        .pipe( remember( config.jsCustomSRC ) ) // Bring all files back to stream.
        .pipe( concat( config.jsCustomFile + '.js' ) )
        .pipe( lineec() ) // Consistent Line Endings for non UNIX systems.
        .pipe( gulp.dest( config.jsCustomDestination ) )
        .pipe(
            rename({
                basename: config.jsCustomFile,
                suffix: '.min'
            })
        )
        .pipe( uglify() )
        .pipe( lineec() ) // Consistent Line Endings for non UNIX systems.
        .pipe( gulp.dest( config.jsCustomDestination ) )
        .pipe( notify({ message: '\n\n✅  ===> CUSTOM JS — completed!\n', onLast: true }) );
});

/**
 * Watch Tasks.
 *
 * Watches for file changes and runs specific tasks.
 */
gulp.task(
    'default',
    gulp.parallel( 'styles', 'vendorsJS', 'customJS', browsersync, () => {
        gulp.watch( config.watchHTML, reload ); // Reload on HTML file changes.
        gulp.watch( config.watchStyles, gulp.parallel( 'styles' ) ); // Reload on SCSS file changes.
        gulp.watch( config.watchJsVendor, gulp.series( 'vendorsJS', reload ) ); // Reload on vendorsJS file changes.
        gulp.watch( config.watchJsCustom, gulp.series( 'customJS', reload ) ); // Reload on customJS file changes.
    })
);