﻿# README #

# Starter HTML

Starter HTML is a front end template for developing robust and latest web standard web apps or sites.

This framework will be frequently updated to go along with latest web standards.

This starter theme is for every web project that is developed for humans and aliens(IE8, IE9 Users) too.

**Contributors:** LDS Ninjas

**Tags:** Sass, Managed File, Typography, Custom-menu



* Lets Start
* Version: 0.1a
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###
Clone the git repo - git clone https://bitbucket.org/lastdoorsolutions/ldsstarter.git

* Summary of Setup
1. HTML5 ready.
2. Use of SASS
3. Useful CSS helper classes
4. Default print styles.

* Configuration
* Dependencies
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

## License ##
Copyright Idealaya Ltd - sampression.com