"use strict";

jQuery(document).ready(function ($) {
  //==============================================================
  // Match Height
  // ==============================================================
  if ($('.equal-box').length > 0) {
    $('.equal-box').matchHeight({
      byRow: 0
    });
  } // Logo Slider


  $('.logo-block__items').slick({
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 5,
    slidesToScroll: 5,
    arrows: false,
    autoplay: true,
    autoplaySpeed: 3000,
    responsive: [{
      breakpoint: 1024,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 4,
        infinite: true
      }
    }, {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    }, {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }]
  });
});